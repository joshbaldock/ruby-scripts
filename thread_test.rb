#!/usr/bin/env ruby
# Simple script to spawn threads and kills them if timeout is reached.
require 'timeout'

timeout = 5
thread_count = 5
threads = []

def command
	i = rand(2)
	if i == 0
		output = `sudo tcpdump -nn -i lo0 -c 1 icmp 2>/dev/null`
	else
		output = `sudo tcpdump -nn -i en0 -c 1 icmp 2>/dev/null`
	end
	#puts output
end

puts "starting #{thread_count} threads"
thread_count.times {
	threads << Thread.new{ command } 
}

threads.each do |thread|
	begin
		Timeout::timeout(timeout) {
			thread.join
			puts "returned thread: #{thread}"
		}
	rescue Timeout::Error => e
		puts "killing thread: #{thread}"
		thread.exit
		next
	end
end